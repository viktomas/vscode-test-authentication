// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export async function activate(context: vscode.ExtensionContext) {
  const onSessionChange = new vscode.EventEmitter<any>();
  const sessions: any[] = [];
  vscode.authentication.registerAuthenticationProvider('gl.test', 'GitLab', {
    createSession: async () => {
      const s = {
        id: 'a',
        accessToken: 'abc',
        scopes: ['all'],
        account: { id: 'acc-id', label: 'account' },
      };
      sessions.push(s);
      onSessionChange.fire(s);
      return s;
    },
    onDidChangeSessions: onSessionChange.event,
    getSessions: async () => {
      if (sessions.length === 0) return [];
      sessions[0].accessToken = sessions[0].accessToken + 'x';
      return sessions;
    },
    removeSession: async () => {},
  });
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  let disposable = vscode.commands.registerCommand('auth.auth', async () => {
    await vscode.window.showInformationMessage(
      'ask for session for the first time'
    );

    const session = await vscode.authentication.getSession('gl.test', ['all'], {
      createIfNone: true,
    });

    vscode.window.showInformationMessage(`session1: ${session?.accessToken}`);

    await vscode.window.showInformationMessage(
      'ask for session for the second time'
    );

    const session2 = await vscode.authentication.getSession(
      'gl.test',
      ['all'],
      {
        createIfNone: true,
      }
    );
    vscode.window.showInformationMessage(`session2: ${session2?.accessToken}`);
  });

  context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() {}
